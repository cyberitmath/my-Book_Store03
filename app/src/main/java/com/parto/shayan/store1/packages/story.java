package com.parto.shayan.store1.packages;

/**
 * Created by shayan on 12/4/2017.
 */

public class story {
    private String name;
    private String author;
    private long id;
    private Boolean isfavourite;

    public story(String name,String author){
        this.name=name;
        this.author=author;
        this.id=0;
        this.isfavourite=false;
    }
    public story(){
        id=0;
        name="";
        author="";
        isfavourite=false;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setIsfavourite(Boolean isfavourite) {
        this.isfavourite = isfavourite;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public long getId() {
        return id;
    }

    public Boolean getIsfavourite() {
        return isfavourite;
    }

}
