package com.parto.shayan.store1;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;


/**
 * Created by shayan on 11/27/2017.
 */

public class BaseActivity extends AppCompatActivity {
    Context mContext = this;
    Activity mActivity = this;
}
