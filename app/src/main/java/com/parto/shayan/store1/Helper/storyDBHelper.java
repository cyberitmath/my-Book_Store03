package com.parto.shayan.store1.Helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by shayan on 12/4/2017.
 */

public class storyDBHelper extends SQLiteOpenHelper {
    public static final String TABEL_BOOKS="story1";
    public static final int TABEL_VERSION=1;

//  column:

    public static final String KEY_ID="id";
    public static final String KEY_TITLE="title";
    public static final String KEY_TYPE="type";
    public static final String KEY_PAGENO="pagenumber";
    public static final String KEY_VIEWORDER="vieworder";
    public static final String KEY_PARENT="parent";
    public static final String KEY_ISFAV="isfavourite";
    public static final String CMD_CREATE_TABLE_BOOKS="CREATE TABLE `"+TABEL_BOOKS+
           " ( "+
           KEY_ID+" ` INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, " +
           KEY_TITLE+ " ` TEXT NOT NULL, "+
            KEY_TYPE+" ` TEXT NOT NULL, "+
            KEY_PAGENO+" ` INTEGER DEFAULT -1, "+
            KEY_VIEWORDER+" ` INTEGER NOT NULL, "+
             KEY_PARENT+" `INTEGER,"+
              KEY_ISFAV+" ` BLOB DEFAULT 'false' "+
            " ) ";

    //type
    public static final String TYPE_TEXT="text";
    public static final String TYPE_IMAGE="image";
    public static final String TYPE_STORY="story";
    public static final String TYPE_INDEXPIC="picindex";
    public static final String TYPE_AUTHOUR="author";



    public storyDBHelper(Context context) {
        super(context, TABEL_BOOKS, null, TABEL_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CMD_CREATE_TABLE_BOOKS);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IS EXISTS"+"TABLE_BOOKS");
        onCreate(db);


    }
}
