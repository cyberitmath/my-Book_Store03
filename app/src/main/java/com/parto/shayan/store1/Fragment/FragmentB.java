package com.parto.shayan.store1.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parto.shayan.store1.R;

/**
 * Created by shayan on 11/28/2017.
 */

public class FragmentB extends Fragment {
    public static FragmentB fragment;

    public static FragmentB getInstance() {
        if (null==fragment)
            fragment=new FragmentB();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_b, container, false);
        return v;
    }
}
