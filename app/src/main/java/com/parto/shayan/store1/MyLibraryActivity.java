package com.parto.shayan.store1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MyLibraryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_library);
        copy();
    }
    private void copy(){
        try {
            InputStream in=getAssets().open("books2db");
            File dir=new File("/data/data/"+getPackageName()+"/databases");
            dir.mkdir();
            FileOutputStream out=new FileOutputStream(new File(dir."books2db"));
            byte[] buffer=new  byte[1024];
            int len=0;
            while ((len=in.read(buffer))>0){
                out.write(buffer,0,len);
            }
            in.close();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
