package com.parto.shayan.store1.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parto.shayan.store1.R;
import com.squareup.picasso.Picasso;

/**
 * Created by shayan on 11/27/2017.
 */

public class StoreMenuAdapter extends BaseAdapter {
    Context mContext;
    int avatars[];
    String names[];

    public StoreMenuAdapter(Context mContext,int[] avatars,String[] names){
        this.mContext=mContext;
        this.avatars=avatars;
        this.names=names;
    }
    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.menu_list_item, viewGroup, false);
        ImageView menuIcon=(ImageView)rowView.findViewById(R.id.menuIcon);
        Picasso.with(mContext).load(avatars[position]).into(menuIcon);

        TextView menuName=(TextView)rowView.findViewById(R.id.menuName);
        menuName.setText(names[position]);



        return rowView;
    }
}
