package com.parto.shayan.store1.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.parto.shayan.store1.Helper.storyDBHelper;
import com.parto.shayan.store1.packages.story;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shayan on 12/4/2017.
 */

public class StoryDataSource {
    private Context Context;
    private SQLiteDatabase database;
    private SQLiteOpenHelper dphelper;

    public  StoryDataSource(Context Context,SQLiteOpenHelper dphelper){
        this.Context=Context;
        this.database=database;
        this.dphelper=dphelper;
    }
    public void openDb(){
        database=dphelper.getWritableDatabase();
    }
    public void  clodeDb(){
        if (database!=null)
            database.close();
    }
    public List<story>getAllStories(){
        List<story>items=new ArrayList<story>();
        openDb();
        String[] columns={storyDBHelper.KEY_ID,storyDBHelper.KEY_TITLE,storyDBHelper.KEY_ISFAV };
        Cursor cursor=database.query(storyDBHelper.TABEL_BOOKS,
                columns,
                "WHERE"+storyDBHelper.KEY_TYPE+"="+storyDBHelper.TYPE_STORY,
                null,null,null,
                storyDBHelper.KEY_ID,"ASC");
        return items;
    }
}
