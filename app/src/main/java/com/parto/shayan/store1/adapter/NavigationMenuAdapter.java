package com.parto.shayan.store1.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parto.shayan.store1.R;
import com.squareup.picasso.Picasso;

/**
 * Created by shayan on 11/28/2017.
 */

public class NavigationMenuAdapter extends BaseAdapter {
    Context mContext;
    int navigationAvatars[];
    String navigationNames[];

    public NavigationMenuAdapter(Context mContext, int[] navigationAvatars, String[] navigationNames) {
        this.mContext = mContext;
        this.navigationAvatars = navigationAvatars;
        this.navigationNames = navigationNames;
    }

    @Override
    public int getCount() {
        return navigationNames.length;
    }

    @Override
    public Object getItem(int position) {
        return navigationNames[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View naviRowView = LayoutInflater.from(mContext).inflate(R.layout.navigation_list_item, viewGroup, false);
        ImageView naviAvatar=(ImageView)naviRowView.findViewById(R.id.naviAvatar);
        Picasso.with(mContext).load(navigationAvatars[position]).into(naviAvatar);

        TextView naviText=(TextView)naviRowView.findViewById(R.id.naviText);
        naviText.setText(navigationNames[position]);

        return naviRowView;
    }
}
