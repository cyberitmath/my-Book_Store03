package com.parto.shayan.store1;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.astuetz.PagerSlidingTabStrip;
import com.parto.shayan.store1.adapter.StoreAdapter;

public class StoreActivity extends AppCompatActivity {
    ViewPager storePager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        storePager=(ViewPager)findViewById(R.id.storePager);



        StoreAdapter adapter=new StoreAdapter(getSupportFragmentManager(),this);
        storePager.setAdapter(adapter);

        PagerSlidingTabStrip tabs=(PagerSlidingTabStrip)findViewById(R.id.tabs);
        tabs.setViewPager(storePager);

    }
}
