package com.parto.shayan.store1;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.parto.shayan.store1.adapter.NavigationMenuAdapter;
import com.parto.shayan.store1.adapter.StoreMenuAdapter;

public class GrideMenuActivity extends BaseActivity {
    GridView gridView;
    ListView navigationListView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gride_menu);
        navigationListView = (ListView) findViewById(R.id.navigationListView);

        gridView = (GridView) findViewById(R.id.gridView);
        String names[] = {
                getString(R.string.myLibrary),
                getString(R.string.store),
                getString(R.string.news),
                getString(R.string.support)
        };
        int avatars[] = {
                R.drawable.libraryy,
                R.drawable.store,
                R.drawable.news,
                R.drawable.support

        };
        final String navigationNames[] = {
                getString(R.string.hasabKarbari),
                getString(R.string.bookStore),
                getString(R.string.myLibrary),
                getString(R.string.modiriyatDanlowdha),
                getString(R.string.sefareshat),
                getString(R.string.setting),
                getString(R.string.aboutUs)
        };
        final int navigationAvatars[] = {
                R.drawable.action_bar_profile,
                R.drawable.action_bar_shop,
                R.drawable.ic_book,
                R.drawable.action_bar_cloud_sync,
                R.drawable.ic_article,
                R.drawable.action_bar_config_read,
                R.drawable.action_bar_book_info
        };

        StoreMenuAdapter adapter = new StoreMenuAdapter(mContext, avatars, names);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String clickedItem1 = (String) adapterView.getItemAtPosition(position);
                if (clickedItem1 == getString(R.string.store)) {
                    Intent intent = new Intent(mContext, StoreActivity.class);
                    startActivity(intent);
                }
                if (clickedItem1 == getString(R.string.myLibrary)) {
                    Intent intent = new Intent(mContext, MyLibraryActivity.class);
                    startActivity(intent);
                }
                if (clickedItem1 == getString(R.string.news)) {
                    Intent intent = new Intent(mContext, NewsActivity.class);
                    startActivity(intent);
                }
                if (clickedItem1 == getString(R.string.support)) {
                    Intent intent = new Intent(mContext, SupportActivity.class);
                    startActivity(intent);
                }
            }
        });


        NavigationMenuAdapter adapter2 = new NavigationMenuAdapter(mContext, navigationAvatars, navigationNames);
        navigationListView.setAdapter(adapter2);

        navigationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String clickedItem = (String) adapterView.getItemAtPosition(position);
                if (clickedItem == getString(R.string.hasabKarbari)) {
                    Intent intent = new Intent(mContext, HesabkarbariActivity.class);
                    startActivity(intent);
                }

                if (clickedItem == getString(R.string.bookStore)) {
                    Intent intent = new Intent(mContext, StoreActivity.class);
                    startActivity(intent);
                }

                if (clickedItem == getString(R.string.myLibrary)) {
                    Intent intent = new Intent(mContext, MyLibraryActivity.class);
                    startActivity(intent);
                }
                if (clickedItem == getString(R.string.modiriyatDanlowdha))
                    Toast.makeText(mContext, R.string.modiriyatDanlowdha, Toast.LENGTH_SHORT).show();
                if (clickedItem == getString(R.string.sefareshat))
                    Toast.makeText(mContext, R.string.sefareshat, Toast.LENGTH_SHORT).show();
                if (clickedItem == getString(R.string.setting))
                    Toast.makeText(mContext, R.string.setting, Toast.LENGTH_SHORT).show();
                if (clickedItem == getString(R.string.aboutUs)) {
                    Intent intent = new Intent(mContext, AboutUs.class);
                    startActivity(intent);
                }

            }
        });

    }

}
