package com.parto.shayan.store1.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.parto.shayan.store1.Fragment.FragmentA;
import com.parto.shayan.store1.Fragment.FragmentB;
import com.parto.shayan.store1.Fragment.FragmentC;
import com.parto.shayan.store1.Fragment.FragmentD;
import com.parto.shayan.store1.R;

/**
 * Created by shayan on 11/28/2017.
 */

public class StoreAdapter extends FragmentPagerAdapter {
    Context mContext;
    public StoreAdapter(FragmentManager fm , Context mContext) {
        super(fm);
        this.mContext=mContext;
    }

    @Override
    public Fragment getItem(int position) {
        if (position==0)
            return FragmentA.getInstance();
        if (position==1)
            return FragmentB.getInstance();
        if (position==2)
            return FragmentC.getInstance();
        if (position==3)
            return FragmentD.getInstance();
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0)
            return mContext.getString(R.string.nonahalan);
        if (position==1)
            return mContext.getString(R.string.shear);
        if (position==2)
            return mContext.getString(R.string.rayegan);
        if (position==3)
            return mContext.getString(R.string.vitrin);
        return null;
    }
}
